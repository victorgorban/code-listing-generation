import { AlignmentType, BorderStyle } from "docx";

export let noBorders = {
  bottom: {
    style: BorderStyle.DOUBLE,
    size: 2,
    color: "white",
  },
  top: {
    style: BorderStyle.DOUBLE,
    size: 2,
    color: "white",
  },
  left: {
    style: BorderStyle.DOUBLE,
    size: 2,
    color: "white",
  },
  right: {
    style: BorderStyle.DOUBLE,
    size: 2,
    color: "white",
  },
};

export function docInfo(docTitle = "", options = {}) {
  return {
    title: docTitle,
    styles: {
      paragraphStyles: [
        {
          id: "Heading1",
          name: "Heading 1",
          basedOn: "Normal",
          next: "Normal",
          quickFormat: true,
          run: {
            size: 32,
            bold: true,
            italics: true,
          },
          paragraph: {
            alignment: AlignmentType.CENTER,
            spacing: {
              // before: 240,
              // after: 120,
            },
          },
        },
        {
          id: "Heading2",
          name: "Heading 2",
          basedOn: "Normal",
          next: "Normal",
          quickFormat: true,
          run: {
            size: 32,
            bold: true,
            italics: true,
          },
          paragraph: {
            alignment: AlignmentType.START,
            spacing: {
              before: 240,
              after: 120,
            },
          },
        },
        {
          id: "Heading1NoEmphasis",
          name: "Heading1 without emphasis",
          basedOn: "Normal",
          next: "Normal",
          quickFormat: true,
          run: {
            size: 32,
          },
          paragraph: {
            alignment: AlignmentType.CENTER,
            spacing: {
              // before: 240,
              // after: 120,
            },
          },
        },
        {
          id: "Default14",
          name: "Default 14pt",
          basedOn: "Normal",
          next: "Normal",
          quickFormat: true,
          run: {
            font: "Times New Roman",
            size: 24, // при открытии размер на 2 делится почему-то
            // bold: true,
          },
        },
        {
          id: "Bold14",
          name: "Bold 14pt",
          basedOn: "Normal",
          next: "Normal",
          quickFormat: true,
          run: {
            font: "Times New Roman",
            size: 28, // при открытии размер на 2 делится почему-то
            bold: true,
          },
        },
        {
          id: "TableCellDefault",
          name: "Table cell default 14pt",
          basedOn: "Normal",
          next: "Normal",
          quickFormat: true,
          run: {
            font: "Times New Roman",
            size: 24, // при открытии размер на 2 делится почему-то
            // bold: true,
          },
          paragraph: {
            alignment: AlignmentType.CENTER,
          },
        },
        {
          id: "TableCellHeader",
          name: "Table cell header 14pt",
          basedOn: "Normal",
          next: "Normal",
          quickFormat: true,
          run: {
            font: "Times New Roman",
            size: 24, // при открытии размер на 2 делится почему-то
            bold: true,
          },
          paragraph: {
            alignment: AlignmentType.CENTER,
          },
        },
        {
          id: "code",
          name: "code",
          basedOn: "Normal",
          next: "Normal",
          quickFormat: true,
          run: {
            font: "Consolas",
            size: 16, // при открытии размер на 2 делится почему-то
            bold: false,
          },
          paragraph: {
            alignment: AlignmentType.START,
          },
        },
      ],
    },
    ...options
  };
}
