import generateListingWord from "./generateReport";

let listingFolder = `E:/WEB/Performstars/Performstars_mobile/lib/`; // absolute path
let saveFilePath = "E:/WEB/Performstars//Mobile_app_Листинг.docx"; // absolute or relative file path. If falsey, then save to ./listing.docx
let docCaption = "Листинг программы Performstars_mobile";
let dirPrefix = "lib";

generateListingWord(listingFolder, saveFilePath, docCaption, dirPrefix)
  .then(() => console.log("Генерация окончена"))
  .catch((e) => {
    console.error(e);
  });
