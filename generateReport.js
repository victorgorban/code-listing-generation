import fs from "fs-extra";
import path from "path";
import walkDir from "klaw";
import * as docx from "docx";
import { docInfo, noBorders } from "./reportWordHelpers";
// returns base64
export default async function generateWordListing(
  listingFolder,
  saveFilePath,
  docCaption,
  dirPrefix = "src"
) {
  listingFolder = path.resolve(listingFolder); // заменяет слеши на правильные, а также убирает слеш в конце

  let documentBuffer = await makeWord();

  await fs.promises.writeFile(saveFilePath, documentBuffer);
  console.log("Файл сохранен ", saveFilePath);
  //   console.log('documentBuffer', documentBuffer);
  // let documentBase64 = documentBuffer.toString("base64");
  // console.log("generate word, saved to", saveFilePath);
  // return documentBase64;
  return null;

  async function makeWord(docTitle) {
    let docData = [];

    docData.push(
      new docx.Paragraph({
        text: docCaption,
        style: "Default14",
      })
    );

    docData.push(
      new docx.Paragraph({
        text: "",
        style: "Default14",
      })
    );

    await fs.mkdir(listingFolder, { recursive: true });

    let paths = [];

    let allowedExts = [".js", ".jsx", ".ts", ".tsx", '.ejs', '.dart'];
    let filterHiddenDirs = (item) => {
      const basename = path.basename(item);
      return basename === "." || basename[0] !== ".";
    };

    for await (const file of walkDir(listingFolder, {
      filter: filterHiddenDirs,
    })) {
      let ext = path.extname(file.path);
      if (!file.stats.isDirectory() && allowedExts.includes(ext))
        paths.push(file.path);
    }

    for (let filePath of paths) {
      let filePathToDisplay = filePath.replace(listingFolder, dirPrefix);
      let fileContent = await fs.readFile(filePath, "utf8");
      docData.push(
        new docx.Paragraph({
          text: filePathToDisplay,
          style: "Heading2",
          heading: docx.HeadingLevel.HEADING_2,
        })
      );
      let fileContentParagraphsChunks = fileContent.split(/\r?\n|\r|\n/g);
      // console.log('fileContentParagraphsChunks 1', fileContentParagraphsChunks)
      // убираем чистые переносы строк, чтобы немного сэкономить место
      fileContentParagraphsChunks = fileContentParagraphsChunks.filter(
        (chunk) => chunk
      );

      let paragraphChildren = fileContentParagraphsChunks.map(
        (textChunk) =>
          new docx.Paragraph({
            text: textChunk,
            style: "code",
          })
      );

      docData.push(
        ...paragraphChildren,
      );
      // console.log("filePath", filePathToDisplay);
    }

    let docInfoData = docInfo(docTitle, {
      sections: [
        {
          properties: {},
          children: docData,
        },
      ],
    });
    // console.log("docInfoData", docInfoData);

    const doc = new docx.Document(docInfoData);
    // console.log("doc created");

    let docBuff = await docx.Packer.toBuffer(doc);
    return docBuff;
  }
}
